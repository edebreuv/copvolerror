=================================================================================================
CopVolError: Copepod Volume Estimation Errors made by the ESD-based and the Ellipse-based Methods
=================================================================================================

REQUIREMENTS
============

Python 3.8 or newer.
Third-party libraries: Matplotlib, Numpy, Scikit-Image, Scipy.



USAGE
=====

Download ``cop-vol-error.py`` and run: ``python cop-vol-error.py``.

The usage help is printed when running:

``python cop-vol-error.py -h``



Thanks
======

The interface has been improved based on an advice by Cédric Dubois.

The project is developed with `PyCharm Community <https://www.jetbrains.com/pycharm>`_.

The code is formatted by `Black <https://github.com/psf/black>`_, *The Uncompromising Code Formatter*.

The imports are ordered by `isort <https://github.com/timothycrosley/isort>`_... *your imports, so you don't have to*.
