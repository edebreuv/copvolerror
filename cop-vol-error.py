# Copyright CNRS/Inria/UCA
# Contributor(s): Eric Debreuve (since 2021)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from __future__ import annotations

import dataclasses as dcls
from typing import Callable, ClassVar, Dict, List, Optional, Sequence, Tuple

import numpy as nmpy
from matplotlib import gridspec as grid_spec_t
from matplotlib import pyplot as pypl
from matplotlib.backend_bases import KeyEvent as key_event_t
from matplotlib.backend_bases import MouseButton as button_event_t
from matplotlib.container import BarContainer as bar_container_t
from mpl_toolkits.mplot3d import Axes3D as axes_3d_t
from scipy import ndimage as image_t
from scipy.spatial import ConvexHull as convex_hull_t
from skimage import measure as msre
from skimage.measure import EllipseModel as ellipse_t


array_t = nmpy.ndarray
error_histograms_h = Dict[str, Tuple[bar_container_t, int, List[float]]]


ELLIPSOID_RADII = (50, 20, 15)


@dcls.dataclass(repr=False, eq=False)
class cop_vol_error_plotter_t:

    USAGE: ClassVar[str] = (
        "Press SPACE BAR to toggle between\n"
        "One Shot and Continuous modes\n"
        "Press the DELETE KEY to clear histograms\n"
        "Press the Q KEY to quit"
    )
    DOMAIN_MARGIN: ClassVar[int] = 3
    MARCHING_CUBE_SMOOTHING: ClassVar[int] = 0
    MARCHING_CUBE_STEP: ClassVar[float] = 3
    ERROR_SEPARATOR: ClassVar[str] = ":"
    ESD_HISTOGRAM_LABEL: ClassVar[str] = "ESD Error"
    ELL_HISTOGRAM_LABEL: ClassVar[str] = "ELL Error"

    radii: Sequence[float] = None

    one_shot_mode: bool = dcls.field(init=False, default=True)
    mode_indicator: pypl.Text = dcls.field(init=False, default=None)
    event_callbacks: Dict[str, Optional[Callable]] = dcls.field(
        init=False, default=None
    )

    aligned_ellipsoid_iso: Sequence[array_t, array_t] = dcls.field(
        init=False, default=None
    )
    volume_prms: float = dcls.field(init=False, default=None)
    volume_map: float = dcls.field(init=False, default=None)

    shape: Sequence[int] = dcls.field(init=False, default=None)
    error_bins: array_t = dcls.field(init=False, default=None)
    histograms: error_histograms_h = dcls.field(init=False, default_factory=dict)
    axes_ellipsoid: axes_3d_t = dcls.field(init=False, default=None)
    axes_error_esd: pypl.Axes = dcls.field(init=False, default=None)
    axes_error_ell: pypl.Axes = dcls.field(init=False, default=None)
    dashboard: pypl.Text = dcls.field(init=False, default=None)

    def __post_init__(self):
        """"""
        event_callbacks = {
            _key: None
            for _key in (
                "button_press_event",
                "motion_notify_event",
                "button_release_event",
            )
        }

        length = int(nmpy.ceil(2 * self.radii[0] + self.__class__.DOMAIN_MARGIN).item())
        shape = 3 * (length,)

        ellipsoid_map = AlignedEllipsoidMap(self.radii, shape)
        vertices, triangles = MeshFromMap(
            ellipsoid_map,
            self.__class__.MARCHING_CUBE_STEP,
            smoothing=self.__class__.MARCHING_CUBE_SMOOTHING,
        )

        volume_prms = VolumeEllipsoidFromParameters(self.radii)
        volume_map = VolumeEllipsoidFromMap(ellipsoid_map)

        min_esd_error, max_esd_error = ExtremeESDErrors(self.radii)
        min_ell_error, max_ell_error = ExtremeELLErrors(self.radii)
        min_error = min(min_esd_error, min_ell_error)
        max_error = max(max_esd_error, max_ell_error)
        error_bins = nmpy.linspace(min_error, max_error, num=51)

        self.event_callbacks = event_callbacks
        self.aligned_ellipsoid_iso = (vertices, triangles)
        self.volume_prms = volume_prms
        self.volume_map = volume_map
        self.shape = shape
        self.error_bins = error_bins

    @classmethod
    def NewFromRadii(
        cls,
        radii: Sequence[float],
        /,
    ) -> cop_vol_error_plotter_t:
        """"""
        if nmpy.any(nmpy.diff(radii) > 0.0):
            raise ValueError(f"{radii}: Radii must be passed in decreasing order")

        instance = cls(radii=radii)

        figure = pypl.figure(figsize=(9, 4))
        axes_ellipsoid = figure.add_subplot(1, 3, 1, projection=axes_3d_t.name)
        axes_error_esd = figure.add_subplot(1, 3, 2)
        axes_error_ell = figure.add_subplot(1, 3, 3)
        mode_indicator = pypl.figtext(0.0, 0.1, "One Shot", va="bottom")
        dashboard = pypl.figtext(0.0, 0.0, "", va="bottom")

        grid = grid_spec_t.GridSpec(
            1,
            3,
            left=0.0,
            right=0.95,
            bottom=0.075,
            top=0.925,
            wspace=0,
            width_ratios=(0.4, 0.3, 0.3),
        )
        axes_ellipsoid.set_position(grid[0].get_position(figure))
        axes_error_esd.set_position(grid[1].get_position(figure))
        axes_error_ell.set_position(grid[2].get_position(figure))

        axes_ellipsoid.set_proj_type("ortho")

        instance.axes_ellipsoid = axes_ellipsoid
        instance.axes_error_esd = axes_error_esd
        instance.axes_error_ell = axes_error_ell
        instance.mode_indicator = mode_indicator
        instance.dashboard = dashboard

        pypl.figtext(0.0, 1.0, cls.USAGE, va="top")
        PlotIsosurface(
            instance.shape,
            instance.aligned_ellipsoid_iso[0],
            instance.aligned_ellipsoid_iso[1],
            instance.radii,
            instance.axes_ellipsoid,
        )
        for axis in range(3):
            HidePane(axis, axes_ellipsoid)
        HideTicks(axes_ellipsoid)
        PrepareErrorHistograms(
            radii, cls.ERROR_SEPARATOR, axes_error_esd, axes_error_ell
        )

        _ = figure.canvas.mpl_connect("key_press_event", instance.DealWithKeyPress)
        instance.event_callbacks["button_release_event"] = figure.canvas.mpl_connect(
            "button_release_event", instance.UpdateErrorHistograms
        )

        return instance

    def UpdateErrorHistograms(self, _: button_event_t, /) -> None:
        """"""
        projected = ProjectedPoints(self.aligned_ellipsoid_iso[0], self.axes_ellipsoid)
        volume_esd, volume_ell = VolumesFromProjectedPoints(projected)
        self._UpdateErrorHistograms(volume_esd, volume_ell)

    def _UpdateErrorHistograms(self, volume_esd: float, volume_ell: float, /) -> None:
        """"""
        dashboard_info = []

        for label, volume, axes in zip(
            (self.__class__.ESD_HISTOGRAM_LABEL, self.__class__.ELL_HISTOGRAM_LABEL),
            (volume_esd, volume_ell),
            (self.axes_error_esd, self.axes_error_ell),
        ):
            error = volume / self.volume_prms
            self._UpdateErrorHistogram(label, error, axes)
            dashboard_info.append(f"{label}: {error:.2f}")

        self.dashboard.set_text("\n".join(dashboard_info))

    def _UpdateErrorHistogram(
        self,
        histogram_label: str,
        new_error: float,
        axes: pypl.Axes,
        /,
    ) -> None:
        """"""
        if histogram_label in self.histograms:
            histogram, max_height, errors = self.histograms[histogram_label]
            errors.append(new_error)

            # Note the "- 1" (see digitize documentation)
            where_insertion = nmpy.digitize((new_error,), self.error_bins).item() - 1
            where_insertion = max(where_insertion, 0)
            where_insertion = min(where_insertion, histogram.patches.__len__() - 1)
            histogram_bin = histogram.patches[where_insertion]
            new_height = int(histogram_bin.get_height()) + 1
            histogram_bin.set_height(new_height)

            if new_height > max_height:
                self.histograms[histogram_label] = (histogram, new_height, errors)
                axes.set_ylim(0, new_height + 1)
                axes.set_yticks(range(new_height + 1))
        else:
            _, _, histogram = axes.hist(
                (new_error,), bins=self.error_bins, lw=1, ec="blue", fc="red"
            )
            errors = [new_error]

            self.histograms[histogram_label] = (histogram, 1, errors)

            axes.set_ylim(0, 2)
            axes.set_yticks((0, 1))

        separator = self.__class__.ERROR_SEPARATOR
        title_prefix = axes.get_title().split(separator)[0]
        axes.set_title(f"{title_prefix}{separator} {nmpy.mean(errors):.2f}")

        self.DrawIdle()

    def ClearHistograms(self) -> None:
        """"""
        for label, axes in zip(
            (
                self.__class__.ESD_HISTOGRAM_LABEL,
                self.__class__.ELL_HISTOGRAM_LABEL,
            ),
            (self.axes_error_esd, self.axes_error_ell),
        ):
            histogram, max_height, errors = self.histograms[label]
            for histogram_bin in histogram.patches:
                histogram_bin.set_height(0)
            self.histograms[label] = (histogram, 0, [])

        self.DrawIdle()

    def DrawIdle(self) -> None:
        """"""
        self.axes_ellipsoid.figure.canvas.draw_idle()

    def ActivateContinuousMode(self, _: button_event_t, /) -> None:
        """"""
        canvas = self.axes_ellipsoid.figure.canvas
        self.event_callbacks["motion_notify_event"] = canvas.mpl_connect(
            "motion_notify_event", self.UpdateErrorHistograms
        )
        self.event_callbacks["button_release_event"] = canvas.mpl_connect(
            "button_release_event", self.DeactivateContinuousMode
        )

    def DeactivateContinuousMode(self, _: button_event_t, /) -> None:
        """"""
        canvas = self.axes_ellipsoid.figure.canvas
        canvas.mpl_disconnect(self.event_callbacks["motion_notify_event"])
        canvas.mpl_disconnect(self.event_callbacks["button_release_event"])

    def DealWithKeyPress(self, event: key_event_t, /) -> None:
        """"""
        if (key := event.key.lower()) == " ":
            canvas = self.axes_ellipsoid.figure.canvas
            if self.one_shot_mode:
                disconnect = "button_release_event"
                connect = "button_press_event"
                action = self.ActivateContinuousMode
            else:
                disconnect = "button_press_event"
                connect = "button_release_event"
                action = self.UpdateErrorHistograms
            canvas.mpl_disconnect(self.event_callbacks[disconnect])
            self.one_shot_mode = not self.one_shot_mode
            self.event_callbacks[connect] = canvas.mpl_connect(connect, action)
            if self.one_shot_mode:
                mode = "One Shot"
            else:
                mode = "Continuous"
            self.mode_indicator.set_text(mode)
            self.DrawIdle()
        elif key == "delete":
            self.ClearHistograms()
        elif key == "q":
            pypl.close(self.axes_ellipsoid.figure)


def VolumeEllipsoidFromParameters(radii: Sequence[float], /) -> float:
    """"""
    return (4.0 / 3.0) * nmpy.pi * nmpy.prod(radii)


def VolumeEllipsoidFromMap(binary_map: array_t, /) -> float:
    """"""
    return nmpy.count_nonzero(binary_map)


def ProjectedPoints(points_3d: array_t, axes: axes_3d_t, /) -> array_t:
    """"""
    theta = -axes.elev * nmpy.pi / 180.0
    phi = -axes.azim * nmpy.pi / 180.0

    cos_angle = nmpy.cos(theta)
    sin_angle = nmpy.sin(theta)
    rot_theta = nmpy.array(
        (
            (cos_angle, 0.0, -sin_angle),
            (0.0, 1.0, 0.0),
            (sin_angle, 0.0, cos_angle),
        )
    )
    cos_angle = nmpy.cos(phi)
    sin_angle = nmpy.sin(phi)
    rot_phi = nmpy.array(
        (
            (cos_angle, -sin_angle, 0.0),
            (sin_angle, cos_angle, 0.0),
            (0.0, 0.0, 1.0),
        )
    )
    rotation = rot_theta @ rot_phi

    output = nmpy.dot(rotation, points_3d.T)[1:].T

    return output


def BestEllipseForPointCloud(points: array_t, /) -> Sequence[float]:
    """"""
    hull = convex_hull_t(points)
    hull_vertices = points[hull.vertices, :]
    ellipse = ellipse_t()
    ellipse.estimate(hull_vertices)

    return ellipse.params


def VolumesFromProjectedPoints(projected: array_t, /) -> Sequence[float]:
    """"""
    parameters = BestEllipseForPointCloud(projected)
    # Note: the classical terminology semi_m??or_axis is replaced with semi_radius_? here (and in other places
    # as well). Actually, the semi-axes returned by EllipseModel can be in any order.
    _, _, semi_radius_1, semi_radius_2, _ = parameters

    volume_esd = VolumeESDFromParameters(semi_radius_1, semi_radius_2)
    volume_ell = VolumeELLFromParameters(semi_radius_1, semi_radius_2)

    return volume_esd, volume_ell


def VolumeESDFromParameters(semi_radius_1: float, semi_radius_2: float, /) -> float:
    """"""
    area = nmpy.pi * (semi_radius_1 - 1.0) * (semi_radius_2 - 1.0)
    eq_radius = nmpy.sqrt(area / nmpy.pi)

    return (4.0 / 3.0) * nmpy.pi * (eq_radius ** 3)


def VolumeELLFromParameters(semi_radius_1: float, semi_radius_2: float, /) -> float:
    """"""
    semi_major_radius = max(semi_radius_1, semi_radius_2) - 1.0
    semi_minor_radius = min(semi_radius_1, semi_radius_2) - 1.0

    return (4.0 / 3.0) * nmpy.pi * (semi_major_radius * semi_minor_radius ** 2)


def ExtremeESDErrors(radii: Sequence[float], /) -> Sequence[float]:
    """"""
    min_error = nmpy.sqrt(nmpy.prod(radii[1:])) / radii[0]
    max_error = nmpy.sqrt(nmpy.prod(radii[:-1])) / radii[2]

    return min_error, max_error


def ExtremeELLErrors(radii: Sequence[float], /) -> Sequence[float]:
    """"""
    min_error = radii[2] / radii[0]
    max_error = radii[1] / radii[2]

    return min_error, max_error


def AlignedEllipsoidMap(radii: Sequence[float], shape: Sequence[int], /) -> array_t:
    """"""
    values = nmpy.zeros(shape, dtype=nmpy.float64)
    center = 0.5 * nmpy.array(shape, dtype=nmpy.float64)
    all_coords = nmpy.meshgrid(*(range(_lgt) for _lgt in shape), indexing="ij")
    for d_idx in range(3):
        values_for_dim = ((all_coords[d_idx] - center[d_idx]) / radii[d_idx]) ** 2
        nmpy.add(values_for_dim, values, out=values)

    output = values < 1.0

    return output


def MeshFromMap(
    binary_map: array_t, step: float, /, *, smoothing: int = None
) -> Sequence[array_t]:
    """"""
    if (smoothing is None) or (smoothing < 2):
        smoothed = binary_map
    else:
        smoothing = 2 * (smoothing // 2) + 1
        smoothed = image_t.uniform_filter(binary_map, size=smoothing)

    vertices, triangles, *_ = msre.marching_cubes(
        smoothed,
        level=0.5,
        step_size=step,
    )

    return vertices, triangles


def PlotIsosurface(
    shape: Sequence[int],
    vertices: array_t,
    triangles: array_t,
    arrow_lengths: Sequence[float],
    axes: axes_3d_t,
    /,
    *,
    scaling: float = 1.5,
) -> None:
    """"""
    _ = axes.plot_trisurf(
        vertices[:, 0],
        vertices[:, 1],
        triangles,
        vertices[:, 2],
        color="b",
        lw=1,
    )
    # center = 0.5 * nmpy.array(shape, dtype=nmpy.float64)
    # origins = nmpy.tile(center, (3, 1)).T
    # axes.quiver(
    #     *origins,
    #     (scaling * arrow_lengths[0], 0, 0),
    #     (0, scaling * arrow_lengths[1], 0),
    #     (0, 0, scaling * arrow_lengths[2]),
    #     color=("r", "g", "b"),
    # )
    # The above code should produce the expected result, I think... But not with Matplotlib: the arrow heads do not get
    # the same color as the main line. There is of course a "good" reason for that, which I do not want to waste time
    # discovering. So instead, the 3 arrows are plotted separately below.
    # The arrow origin is also shifted since Matplotlib cannot deal with partially hidden parts.
    for a_idx, color in enumerate(("r", "g", "b")):
        arrow_tip = (
            scaling * arrow_lengths[_idx] if _idx == a_idx else 0.0 for _idx in range(3)
        )
        axes.quiver(
            0,
            0,
            0,
            *arrow_tip,
            color=color,
        )
    axis_names = ("x", "y", "z")
    for d_idx in range(3):
        SetAxisLimits = getattr(axes, f"set_{axis_names[d_idx]}lim3d")
        SetAxisLimits(0, shape[d_idx])
    axes.set_box_aspect(shape)

    axes.figure.canvas.draw()  # Sets axes.M to get_proj() (not draw_idle)


def HidePane(axis: int, axes: axes_3d_t, /) -> None:
    """"""
    axis_name = ("x", "y", "z")[axis]
    pane = getattr(axes, f"w_{axis_name}axis")

    pane.set_pane_color((1.0, 1.0, 1.0, 0.0))  # Hides pane


def HideTicks(axes: axes_3d_t, /) -> None:
    """"""
    for name in ("x", "y", "z"):
        SetTicks = getattr(axes, f"set_{name}ticks")
        SetTicks(())
        pane = getattr(axes, f"w_{name}axis")
        pane.line.set_color((1.0, 1.0, 1.0, 0.0))  # Hides spine


def PrepareErrorHistograms(
    radii: Sequence[float],
    separator: str,
    axes_error_esd: pypl.Axes,
    axes_error_ell: pypl.Axes,
) -> None:
    """"""
    min_esd_error, max_esd_error = ExtremeESDErrors(radii)
    min_ell_error, max_ell_error = ExtremeELLErrors(radii)

    for axes, title in zip(
        (axes_error_esd, axes_error_ell),
        (f"ESD/True{separator}", f"Ellipse/True{separator}"),
    ):
        axes.set_title(title)

        if axes is axes_error_esd:
            bounds = (min_esd_error, max_esd_error)
        else:
            bounds = (min_ell_error, max_ell_error)
        for bound in bounds:
            axes.vlines(bound, 0.0, 1.0, colors="g")
            annotation = axes.annotate(
                f"{bound:.2f}", (bound, 1.02), ha="center", color="g"
            )
            annotation.set_rotation("vertical")

        axes.set_ylim(0, 1.2)

    axes_error_ell.yaxis.tick_right()


def Main(
    radii: Sequence[float],
    /,
) -> None:
    """"""
    error_plotter = cop_vol_error_plotter_t.NewFromRadii(radii)
    print(
        f"Ellipsoid Domain: {error_plotter.shape}\n"
        f"Mesh: V.{error_plotter.aligned_ellipsoid_iso[0].shape[0]} / "
        f"T.{error_plotter.aligned_ellipsoid_iso[1].shape[0]}\n"
        f"Volume: Exact.{error_plotter.volume_prms:.2f} / "
        f"Discrete.{error_plotter.volume_map}"
    )

    pypl.show()


if __name__ == "__main__":
    #
    import sys as sstm
    import argparse as cmdl

    parser = cmdl.ArgumentParser(
        epilog="Note that the arguments must respect the following constraints:\n"
        "    - R1 >= R2 >= R3, and consequently\n"
        "    - R3/R1 <= R2/R1 <= 1",
        formatter_class=cmdl.RawDescriptionHelpFormatter,
    )
    parser.add_argument(
        "r1_OR_r2_over_r1",
        type=float,
        nargs="?",
        help="Largest radius R1, or ratio R2/R1",
    )
    parser.add_argument(
        "r2_OR_r3_over_r1",
        type=float,
        nargs="?",
        help="Middle radius R2, or ratio R3/R1",
    )
    parser.add_argument("r3", type=float, nargs="?", help="Smallest radius R3")
    arguments = parser.parse_args()

    radii = (arguments.r1_OR_r2_over_r1, arguments.r2_OR_r3_over_r1, arguments.r3)
    if radii[2] is None:
        if all(_rds is None for _rds in radii[:2]):
            radii = ELLIPSOID_RADII
        elif any(_rds is None for _rds in radii[:2]) or any(
            _rds > 1.0 for _rds in radii[:2]
        ):
            parser.print_help()
            sstm.exit(-1)
        else:
            radii = (50.0, 50.0 * radii[0], 50.0 * radii[1])
    elif any(_rds is None for _rds in radii[:2]):
        parser.print_help()
        sstm.exit(-1)
    if any(radii[_idx] < radii[_idx + 1] for _idx in range(2)):
        parser.print_help()
        sstm.exit(-1)

    Main(radii)
